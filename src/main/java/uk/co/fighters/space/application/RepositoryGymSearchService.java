package uk.co.fighters.space.application;

import static org.springframework.util.Assert.notNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.GeoResults;
import org.springframework.stereotype.Service;

import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Gym;
import uk.co.fighters.space.infrastructure.persistance.GymRepository;
import uk.co.fighters.space.infrastructure.rest.GymSearchRequest;
import uk.co.fighters.space.infrastructure.rest.MapArea;

@Service
public class RepositoryGymSearchService implements GymSearchService {

    @Autowired
    private GymRepository gymsRepo;

    //    TODO what should I do about transactionality in the queries??????
//    @Transactional(readOnly = true)
    @Override
    public List<Gym> findAllByActivites(Activity... activities) {
        return gymsRepo.getAllWhereActivityIsIn(activities);
    }

    @Override
    public GeoResults<Gym> findGyms(GymSearchRequest gymSearchRequest) {
        MapArea mapArea = gymSearchRequest.getMapArea();
        notNull(mapArea, "MapArea cannot be null!");

        if (gymSearchRequest.getActivities().isPresent() && gymSearchRequest.getFacilities().isPresent()) {
            return gymsRepo.getNearWithAllActivitiesAndAllFacilities(mapArea.getPoint(), mapArea.getDistance(), gymSearchRequest.getActivities().get(), gymSearchRequest.getFacilities().get());
        } else if (gymSearchRequest.getActivities().isPresent()) {
            return gymsRepo.getNearWithAllActivities(mapArea.getPoint(), mapArea.getDistance(), gymSearchRequest.getActivities().get());
        } else if (gymSearchRequest.getFacilities().isPresent()) {
            return gymsRepo.getNearWithAllFacilities(mapArea.getPoint(), mapArea.getDistance(), gymSearchRequest.getFacilities().get());
        }

        return gymsRepo.findByPositionNear(mapArea.getPoint());
    }

}

