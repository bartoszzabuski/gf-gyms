package uk.co.fighters.space.application;

import java.util.List;

import org.springframework.data.geo.GeoResults;

import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Gym;
import uk.co.fighters.space.infrastructure.rest.GymSearchRequest;

public interface GymSearchService {

    List<Gym> findAllByActivites(Activity... activities);

    GeoResults<Gym> findGyms(GymSearchRequest gymSearchRequest);
}

