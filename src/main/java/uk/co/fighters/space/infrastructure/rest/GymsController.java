package uk.co.fighters.space.infrastructure.rest;


import static java.util.stream.Collectors.toList;
import static org.springframework.data.geo.Metrics.MILES;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static uk.co.fighters.space.domain.Ranking.rankingOf;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import uk.co.fighters.space.application.GymSearchService;
import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Facility;
import uk.co.fighters.space.domain.Gym;
import uk.co.fighters.space.domain.Ranking;
import uk.co.fighters.space.infrastructure.persistance.GymRepository;
import uk.co.fighters.space.infrastructure.rest.dto.RankingResource;
import uk.co.fighters.space.infrastructure.rest.exceptions.GymNotFoundException;

@Controller
@RequestMapping("/gyms")
public class GymsController {

    public static final Point TRAFALGAR_SQUARE_POSITION = new Point(51.5076, -0.1278);
    public static final Distance LONDON_RADIUS = new Distance(21, MILES);
    @Autowired
    private GymRepository gymsRepo;

    @Autowired
    private GymSearchService gymSearchService;

    @RequestMapping(value = "gyms2", method = GET)
    public
    @ResponseBody
    Set<Gym> findGymByName(@RequestParam(value = "name", required = false) String name) {
        Set<Gym> gyms = gymsRepo.findAllByNameIgnoringCase(name);
        if (gyms == null || gyms.isEmpty()) {
            throw new GymNotFoundException(name);
        }
        return gyms;
    }

    @RequestMapping(value = "/{gymId}/ranking", method = POST)
    public
    @ResponseBody
    String updateRating(@PathVariable("gymId") String gymId, @RequestBody() RankingResource rankingResource) {
        Ranking ranking = rankingOf(rankingResource.getName(), rankingResource.getValue());
        System.out.println("---- " + gymId + " - "+ ranking.toString());
        return "booom!";
    }

    @RequestMapping(value = "/london", method = GET)
    @ResponseBody
    List<Gym> findGymBySearchCriteria(@RequestParam(value = "activity", required = false) List<String> activityNames,
                                      @RequestParam(value = "facility", required = false) List<String> facilityNames) {
        MapArea londonMapArea = new MapArea(TRAFALGAR_SQUARE_POSITION, LONDON_RADIUS);
        GeoResults<Gym> geoResults = gymSearchService.findGyms(new GymSearchRequest(londonMapArea, assembleActivities(activityNames), assembleFacilities(facilityNames)));
        return geoResults.getContent().stream().map(GeoResult::getContent).collect(toList());
    }

    private Optional<List<Activity>> assembleActivities(List<String> activityNames) {
        if (activityNames == null || activityNames.isEmpty()) return Optional.empty();
        return Optional.of(activityNames.stream().map(Activity::parseBy).collect(toList()));
    }

    private Optional<List<Facility>> assembleFacilities(List<String> facilityNames) {
        if (facilityNames == null || facilityNames.isEmpty()) return Optional.empty();
        return Optional.of(facilityNames.stream().map(Facility::parseBy).collect(toList()));
    }

//    find gym by name
//    find gym by londonArea
//    find gym by activity types
//    find facility type

}

