package uk.co.fighters.space.infrastructure.rest.dto;

import lombok.Data;

@Data
public class RankingResource {
    private final String name;
    private final Integer value;
}
