package uk.co.fighters.space.infrastructure.rest;


import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;

import lombok.Value;

@Value
public class MapArea {
    private final Point point;
    private final Distance distance;

    MapArea(Point point, Distance distance) {
        this.point = point;
        this.distance = distance;
    }

}
