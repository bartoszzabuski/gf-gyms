package uk.co.fighters.space.infrastructure.rest;

import java.util.List;
import java.util.Optional;

import lombok.Value;
import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Facility;

@Value
public class GymSearchRequest {
    private final MapArea mapArea;
    private final Optional<List<Activity>> activities;
    private final Optional<List<Facility>> facilities;
}
