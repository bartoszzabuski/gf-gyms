package uk.co.fighters.space.infrastructure.rest.exceptions;

import static java.lang.String.format;

public class GymNotFoundException extends RuntimeException {

    public GymNotFoundException(String name) {
        super(format("Gym matching name %s not found!", name));
    }
}
