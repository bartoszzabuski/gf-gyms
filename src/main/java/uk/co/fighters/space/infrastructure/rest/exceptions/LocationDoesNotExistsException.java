package uk.co.fighters.space.infrastructure.rest.exceptions;

import static java.lang.String.format;

public class LocationDoesNotExistsException extends RuntimeException {

    public LocationDoesNotExistsException(String locationName) {
        super(format("Location %s not found", locationName));
    }
}
