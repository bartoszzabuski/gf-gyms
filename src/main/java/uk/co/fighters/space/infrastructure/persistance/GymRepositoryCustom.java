package uk.co.fighters.space.infrastructure.persistance;


import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;

import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Facility;
import uk.co.fighters.space.domain.Gym;

public interface GymRepositoryCustom {

    List<Gym> getAllWhereActivityIsIn(Activity... activities);

    List<Gym> getAllWhereFacilityIsIn(Facility... facilities);

    GeoResults<Gym> getNearWithAllFacilities(Point point, Distance distance, List<Facility> facilities);

    GeoResults<Gym> getNearWithAllActivities(Point point, Distance distance, List<Activity> activities);

    GeoResults<Gym> getNearWithAllActivitiesAndAllFacilities(Point point, Distance distance, List<Activity> activities, List<Facility> facilities);

}

