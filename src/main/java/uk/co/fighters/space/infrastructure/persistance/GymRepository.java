package uk.co.fighters.space.infrastructure.persistance;

import java.util.List;
import java.util.Set;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Facility;
import uk.co.fighters.space.domain.Gym;

//    find gym by name - fizzy!
//    find gym by activity types
//    find facility te
public interface GymRepository extends MongoRepository<Gym, String>, GymRepositoryCustom {

    //    to be removed
    Set<Gym> findAllByNameIgnoringCase(String name);

    Set<Gym> findByNameLikeIgnoringCase(String name);

    Gym insert(Gym gym);

    GeoResults<Gym> findByPositionNear(Point point);

    GeoResults<Gym> findByPositionNearAndActivities(Point point, Distance distance, List<Activity> activities);

    GeoResults<Gym> findByPositionNearAndFacilities(Point point, Distance distance, Facility facility);

    GeoResults<Gym> findByPositionNearAndActivitiesAndFacilities(Point point, Distance distance, Activity activity, Facility facility);

}
