package uk.co.fighters.space.infrastructure.persistance;


import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.util.Assert.notNull;
import static uk.co.fighters.space.infrastructure.persistance.GymRepositoryImpl.SearchQueryBuilder.gymsNearWithAllActivites;
import static uk.co.fighters.space.infrastructure.persistance.GymRepositoryImpl.SearchQueryBuilder.gymsNearWithAllActivitesAndAllFacilities;
import static uk.co.fighters.space.infrastructure.persistance.GymRepositoryImpl.SearchQueryBuilder.gymsNearWithAllFacilities;
import static uk.co.fighters.space.infrastructure.persistance.GymRepositoryImpl.SearchQueryBuilder.gymsWithAnyActivitesFrom;
import static uk.co.fighters.space.infrastructure.persistance.GymRepositoryImpl.SearchQueryBuilder.gymsWithAnyFacilitiesFrom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import uk.co.fighters.space.domain.Activity;
import uk.co.fighters.space.domain.Facility;
import uk.co.fighters.space.domain.Gym;

public class GymRepositoryImpl implements GymRepositoryCustom {

    public static final int MAX_LIMIT = 10000000;
    private final MongoOperations mongoOperations;

    @Autowired
    public GymRepositoryImpl(MongoOperations mongoOperations) {
        notNull(mongoOperations, "MongoOperations must not be null!");
        this.mongoOperations = mongoOperations;
    }

    public List<Gym> getAllWhereActivityIsIn(Activity... activities) {
        return mongoOperations.find(gymsWithAnyActivitesFrom(activities), Gym.class);
    }

    public List<Gym> getAllWhereFacilityIsIn(Facility... facilities) {
        return mongoOperations.find(gymsWithAnyFacilitiesFrom(facilities), Gym.class);
    }

    public GeoResults<Gym> getNearWithAllFacilities(Point point, Distance distance, List<Facility> facilities) {
        return mongoOperations.geoNear(gymsNearWithAllFacilities(point, distance, facilities).num(MAX_LIMIT), Gym.class);
    }

    public GeoResults<Gym> getNearWithAllActivities(Point point, Distance distance, List<Activity> activities) {
        return mongoOperations.geoNear(gymsNearWithAllActivites(point, distance, activities).num(MAX_LIMIT), Gym.class);
    }

    @Override
    public GeoResults<Gym> getNearWithAllActivitiesAndAllFacilities(Point point, Distance distance, List<Activity> activities, List<Facility> facilities) {
        return mongoOperations.geoNear(gymsNearWithAllActivitesAndAllFacilities(point, distance, activities, facilities).num(MAX_LIMIT), Gym.class);
    }

    static class SearchQueryBuilder {

        private static final String ACTIVITIES = "activities";
        private static final String FACILITIES = "facilities";

        private SearchQueryBuilder() {
        }

        // db.getCollection('gyms').find({ "activities" : { $all: [ "BOXING" , "KICKBOXING", "BJJ", "MMA"]}})
        // OR
        // db.getCollection('gyms').find({ activities: { $in : [ "BOXING" ]}})
        static Query gymsWithAnyActivitesFrom(Activity... actvities) {
            return query(where(ACTIVITIES).in(actvities));
        }

        // db.getCollection('gyms').find({ facilities: { $in : [ "PT" ]}})
        static Query gymsWithAnyFacilitiesFrom(Facility... facilities) {
            return query(where(FACILITIES).in(facilities));
        }

        static NearQuery gymsNearWithAllFacilities(Point point, Distance distance, List<Facility> facilities) {
            return NearQuery.near(point.getX(), point.getY()).maxDistance(distance).spherical(true).query(query(where(FACILITIES).all(facilities)));
        }

        static NearQuery gymsNearWithAllActivites(Point point, Distance distance, List<Activity> activities) {
            return NearQuery.near(point.getX(), point.getY()).maxDistance(distance).spherical(true).query(query(where(ACTIVITIES).all(activities)));
        }

        static NearQuery gymsNearWithAllActivitesAndAllFacilities(Point point, Distance distance, List<Activity> activities, List<Facility> facilities) {
            return NearQuery.near(point.getX(), point.getY()).maxDistance(distance).spherical(true).query(query(where(ACTIVITIES).all(activities).andOperator((where(FACILITIES).all(facilities)))));
        }
    }

}

