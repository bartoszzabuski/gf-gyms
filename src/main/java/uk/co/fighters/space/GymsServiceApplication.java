package uk.co.fighters.space;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
//@EnableConfigurationProperties({ResourceProperties.class})
//public class GymsServiceApplication extends WebMvcConfigurerAdapter {
public class GymsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GymsServiceApplication.class, args);
    }

    @Bean
    @Profile(value = "cloud222222321ewe2")
    public ApplicationInstanceInfo applicationInfo() {
        return cloud().getApplicationInstanceInfo();
    }

    @Bean
    @Profile(value = "cloud222222232323")
    public Cloud cloud() {
        CloudFactory cloudFactory = new CloudFactory();
        return cloudFactory.getCloud();
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry
//                .addResourceHandler("/resources/static/src/**")
//                .addResourceLocations("/resources/static/src");
//    }

    @Autowired
    private ResourceProperties resourceProperties = new ResourceProperties();

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
////        Integer cachePeriod = resourceProperties.getCachePeriod();
//
//        registry.addResourceHandler("/**")
//                .addResourceLocations("classpath:/static/dist/");
//
////        registry.addResourceHandler("/**")
////                .addResourceLocations("classpath:/static/src/app");
////                .setCachePeriod(cachePeriod);
//
////        registry.addResourceHandler("src/**")
////                .addResourceLocations("classpath:/static/src/index.html")
//////                .setCachePeriod(cachePeriod)
////                .resourceChain(true)
////                .addResolver(new PathResourceResolver() {
////                    @Override
////                    protected Resource getResource(String resourcePath,
////                                                   Resource location) throws IOException {
////                        return location.exists() && location.isReadable() ? location
////                                : null;
////                    }
////                });
//    }


}


