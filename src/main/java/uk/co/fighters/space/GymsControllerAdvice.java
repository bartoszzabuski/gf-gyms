package uk.co.fighters.space;


import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

import java.util.Optional;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import uk.co.fighters.space.infrastructure.rest.exceptions.GymNotFoundException;
import uk.co.fighters.space.infrastructure.rest.exceptions.LocationDoesNotExistsException;

@ControllerAdvice
//@RequestMapping(produces = "application/vnd.error")
public class GymsControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {GymNotFoundException.class, LocationDoesNotExistsException.class})
    protected ResponseEntity<Object> handleLocationDoesNotExist(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = Optional.of(ex.getMessage()).orElse(ex.getClass().getSimpleName());
//        return new VndErrors("--", bodyOfResponse);
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), BAD_REQUEST, request);
    }

//@ResponseStatus(value = NOT_FOUND)
//@ExceptionHandler(GymNotFoundException.class)
//public VndErrors personNotFoundException(GymNotFoundException e) {
//return this.error(e, "--");
//}
//
//private <E extends Exception> VndErrors error(E e, String logref) {
//String msg = Optional.of(e.getMessage()).orElse(e.getClass().getSimpleName());
//return new VndErrors(logref, msg);
//}

}
