package uk.co.fighters.space.temp;

import static java.util.Arrays.asList;
import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;
import static org.hibernate.validator.internal.util.CollectionHelper.newHashMap;
import static uk.co.fighters.space.domain.Activity.BJJ;
import static uk.co.fighters.space.domain.Activity.BOXING;
import static uk.co.fighters.space.domain.Activity.KARATE;
import static uk.co.fighters.space.domain.Activity.KICKBOXING;
import static uk.co.fighters.space.domain.Activity.MMA;
import static uk.co.fighters.space.domain.Activity.THAI_BOXING;
import static uk.co.fighters.space.domain.CriteriaName.BJJ_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.BOOT_CAMP_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.FACILITIES;
import static uk.co.fighters.space.domain.CriteriaName.FEMALE_FRIENDLY;
import static uk.co.fighters.space.domain.CriteriaName.FUN;
import static uk.co.fighters.space.domain.CriteriaName.GYM_FINDER_SCORE;
import static uk.co.fighters.space.domain.CriteriaName.KARATE_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.KICKBOXING_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.MMA_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.MONEY_FOR_VALUE;
import static uk.co.fighters.space.domain.CriteriaName.OTHER_PARTICIPANTS_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.PRICE;
import static uk.co.fighters.space.domain.CriteriaName.PT_LEVEL;
import static uk.co.fighters.space.domain.CriteriaName.THAI_BOXING_LEVEL;
import static uk.co.fighters.space.domain.Facility.BOOT_CAMP;
import static uk.co.fighters.space.domain.Facility.CARDIO;
import static uk.co.fighters.space.domain.Facility.PT;
import static uk.co.fighters.space.domain.Facility.SWIMMING_POOL;
import static uk.co.fighters.space.domain.Facility.WEIGHTS;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;

import uk.co.fighters.space.domain.CriteriaName;
import uk.co.fighters.space.domain.CriteriaValue;
import uk.co.fighters.space.domain.Gym;
import uk.co.fighters.space.domain.Ranking;
import uk.co.fighters.space.infrastructure.persistance.GymRepository;

//@Profile(value = "dev")
@Component
public class DataLoader implements ApplicationRunner {

    private GymRepository gymsRepo;
    private Map<CriteriaName, CriteriaValue> randomRankings;
    private Random random = new Random();


    @Autowired
    public DataLoader(GymRepository gymsRepo) {
        this.gymsRepo = gymsRepo;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

        gymsRepo.deleteAll();

//        EAST
        gymsRepo.insert(new Gym(ObjectId.get(), "Bob Breen Academy", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper fringilla libero nec rutrum. Cras eu consequat ante. Suspendisse mi quam, tincidunt non eros nec, accumsan", new Point(51.530297, -0.078274), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "east1", "Integer felis justo, varius eget euismod et, eleifend at libero. Ut commodo id augue id ultrices. Proin semper mi sem, sit amet sollicitudin justo bibendum", new Point(51.533519, -0.012518), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, PT, SWIMMING_POOL, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "east2", "Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas id ornare metus. Nunc a eros eleifend, euismod ante vel, malesuada nibh. Curabitur tempor ante velit, vel accumsan metus efficitur eu. Nulla porttitor tempus eleifend. Morbi id massa ut purus euismod tempor. Donec ut", new Point(51.513002, -0.021153), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, WEIGHTS), getRandomRankings()));


//        CENTRAL
        gymsRepo.insert(new Gym(ObjectId.get(), "ThirdSpace", "Integer felis justo, varius eget euismod et, eleifend at libero. Ut commodo id augue id ultrices. Proin semper mi sem, sit amet sollicitudin justo bibendum", new Point(51.511079, -0.135839), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, PT, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "KO something", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper fringilla libero nec rutrum. Cras eu consequat ante. Suspendisse mi quam, tincidunt non eros nec, accumsan", new Point(51.512045, -0.137470), newSet(BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "central1", "Vestibulum ut leo dui. Pellentesque nec lectus a massa vestibulum fermentum a at justo. Praesent at erat sit amet nisl malesuada bibendum. Quisque", new Point(51.510470, -0.144576), newSet(BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));

//        WEST
        gymsRepo.insert(new Gym(ObjectId.get(), "Wembley", "Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas id ornare metus. Nunc a eros eleifend, euismod ante vel, malesuada nibh. Curabitur tempor ante velit, vel accumsan metus efficitur eu. Nulla porttitor tempus eleifend. Morbi id massa ut purus euismod tempor. Donec ut", new Point(51.565391, -0.309010), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, PT, SWIMMING_POOL, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "West1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper fringilla libero nec rutrum. Cras eu consequat ante. Suspendisse mi quam, tincidunt non eros nec, accumsan", new Point(51.523551, -0.362953), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "West2", "Vestibulum ut leo dui. Pellentesque nec lectus a massa vestibulum fermentum a at justo. Praesent at erat sit amet nisl malesuada bibendum. Quisque", new Point(51.514569, -0.373627), newSet(BOXING, KICKBOXING, THAI_BOXING, KARATE), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));

//        SOUTH
        gymsRepo.insert(new Gym(ObjectId.get(), "South1", "Integer felis justo, varius eget euismod et, eleifend at libero. Ut commodo id augue id ultrices. Proin semper mi sem, sit amet sollicitudin justo bibendum", new Point(51.403184, -0.098068), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, PT, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "South2", "Mauris interdum pharetra pharetra. In sed dolor sit amet mi tempor vehicula non vitae elit. Vivamus ac nisl sagittis, condimentum enim vitae, tristique", new Point(51.438319, -0.040041), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "South3", "Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas id ornare metus. Nunc a eros eleifend, euismod ante vel, malesuada nibh. Curabitur tempor ante velit, vel accumsan metus efficitur eu. Nulla porttitor tempus eleifend. Morbi id massa ut purus euismod tempor. Donec ut", new Point(51.462418, -0.110914), newSet(BOXING, KICKBOXING, THAI_BOXING), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));

//        NORTH
        gymsRepo.insert(new Gym(ObjectId.get(), "north1", "Integer felis justo, varius eget euismod et, eleifend at libero. Ut commodo id augue id ultrices. Proin semper mi sem, sit amet sollicitudin justo bibendum", new Point(51.658349, -0.173469), newSet(BOXING, KICKBOXING, MMA, THAI_BOXING), newSet(BOOT_CAMP, CARDIO, PT, SWIMMING_POOL, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "north2", "Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas id ornare metus. Nunc a eros eleifend, euismod ante vel, malesuada nibh. Curabitur tempor ante velit, vel accumsan metus efficitur eu. Nulla porttitor tempus eleifend. Morbi id massa ut purus euismod tempor. Donec ut", new Point(51.655613, -0.085984), newSet(BJJ, BOXING, KICKBOXING, MMA, THAI_BOXING, KARATE), newSet(BOOT_CAMP, CARDIO, WEIGHTS), getRandomRankings()));
        gymsRepo.insert(new Gym(ObjectId.get(), "north3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper fringilla libero nec rutrum. Cras eu consequat ante. Suspendisse mi quam, tincidunt non eros nec, accumsan", new Point(51.679287, -0.098130), newSet(BOXING, KICKBOXING, THAI_BOXING), newSet(BOOT_CAMP, CARDIO), getRandomRankings()));


    }

    private <T> Set<T> newSet(T... elements) {
        return new HashSet<>(asList(elements));
    }

    public Map<CriteriaName, CriteriaValue> getRandomRankingsMap() {
        Map<CriteriaName, CriteriaValue> randomRankings = newHashMap();

        randomRankings.put(GYM_FINDER_SCORE, getRandomRandkingValue());
        randomRankings.put(PRICE, getRandomRandkingValue());
        randomRankings.put(MONEY_FOR_VALUE, getRandomRandkingValue());
        randomRankings.put(FUN, getRandomRandkingValue());
        randomRankings.put(FEMALE_FRIENDLY, getRandomRandkingValue());
        randomRankings.put(FACILITIES, getRandomRandkingValue());
        randomRankings.put(BOOT_CAMP_LEVEL, getRandomRandkingValue());
        randomRankings.put(PT_LEVEL, getRandomRandkingValue());
        randomRankings.put(BJJ_LEVEL, getRandomRandkingValue());
        randomRankings.put(KICKBOXING_LEVEL, getRandomRandkingValue());
        randomRankings.put(MMA_LEVEL, getRandomRandkingValue());
        randomRankings.put(THAI_BOXING_LEVEL, getRandomRandkingValue());
        randomRankings.put(KARATE_LEVEL, getRandomRandkingValue());
        randomRankings.put(OTHER_PARTICIPANTS_LEVEL, getRandomRandkingValue());
        randomRankings.put(PRICE, getRandomRandkingValue());

        return randomRankings;
    }

    public List<Ranking> getRandomRankings() {
        List<Ranking> randomRankings = newArrayList();

        randomRankings.add(new Ranking(GYM_FINDER_SCORE, getRandomUpToFive()));
        randomRankings.add(new Ranking(PRICE, getRandomUpToFive()));
        randomRankings.add(new Ranking(MONEY_FOR_VALUE, getRandomUpToFive()));
        randomRankings.add(new Ranking(FUN, getRandomUpToFive()));
        randomRankings.add(new Ranking(FEMALE_FRIENDLY, getRandomUpToFive()));
        randomRankings.add(new Ranking(FACILITIES, getRandomUpToFive()));
        randomRankings.add(new Ranking(BOOT_CAMP_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(PT_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(BJJ_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(KICKBOXING_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(MMA_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(THAI_BOXING_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(KARATE_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(OTHER_PARTICIPANTS_LEVEL, getRandomUpToFive()));
        randomRankings.add(new Ranking(PRICE, getRandomUpToFive()));

        return randomRankings;
    }

    public CriteriaValue getRandomRandkingValue() {
        return new CriteriaValue(getRandomUpToFive());
    }

    public Integer getRandomUpToFive() {
        return random.nextInt(6);
    }
}
