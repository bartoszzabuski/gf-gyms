package uk.co.fighters.space.temp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile(value = "cloud22222")
public class CloudInfoController {

    @Autowired
    private ApplicationInstanceInfo info;

    @RequestMapping("/cloudInfo")
    public ApplicationInstanceInfo info() {
        return info;
    }

}
