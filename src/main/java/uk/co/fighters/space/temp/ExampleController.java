package uk.co.fighters.space.temp;


import static org.springframework.data.geo.Metrics.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

    public static final Distance ONE_MILE = new Distance(1, MILES);
    public static final Point GAMESYS_COORDINATES = new Point(51.509770, -0.135644);

//    @Autowired
//    private GymRepository gymsRepo;
//
//    @RequestMapping("/gyms")
//    public List<Gym> hello() {
//        return gymsRepo.findAll();
//    }
//
//    @RequestMapping("/gyms/nearest")
//    public GeoResults<Gym> findNearest() {
//        return gymsRepo.findByPositionNear(GAMESYS_COORDINATES, ONE_MILE);
//    }

}
