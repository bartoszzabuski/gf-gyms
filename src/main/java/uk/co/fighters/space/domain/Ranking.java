package uk.co.fighters.space.domain;

import lombok.Value;

@Value
public class Ranking {

    private final CriteriaName name;
    private final Integer value;

    public Ranking(CriteriaName name, Integer value) {
        if (value < 0 || value > 10) {
            throw new RuntimeException("Ranking score can only be between 0 - 10 only");
        }
        this.name = name;
        this.value = value;
    }

    public static Ranking rankingOf(String name, Integer value) {
        return new Ranking(CriteriaName.parse(name), value);
    }
}
