package uk.co.fighters.space.domain;


import static java.lang.String.*;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonValue;

public enum CriteriaName {

    GYM_FINDER_SCORE("GymFinder score"),
    PRICE("price"),
    MONEY_FOR_VALUE("money for value"),
    FUN("fun"),
    FEMALE_FRIENDLY("female friendly"),

    FACILITIES("facilities"),
    BOOT_CAMP_LEVEL("Boot camp level"),
    PT_LEVEL("PTs level"),

    BJJ_LEVEL("BJJ level"),
    KICKBOXING_LEVEL("Kickboxing level"),
    MMA_LEVEL("MMA level"),
    THAI_BOXING_LEVEL("Thai Boxing level"),
    KARATE_LEVEL("Karate level"),
    OTHER_PARTICIPANTS_LEVEL("general group level");

    private String value;

    CriteriaName(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public static CriteriaName parse(String requestedValue) {
        return Stream.of(values())
                .filter(value -> value.getValue().equalsIgnoreCase(requestedValue))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(format("No matching ranking criteria for %s", requestedValue)));
    }
}
