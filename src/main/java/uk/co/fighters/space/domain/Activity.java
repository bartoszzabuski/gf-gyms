package uk.co.fighters.space.domain;


import static java.util.Arrays.stream;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Activity {

    BJJ("BJJ"),
    BOXING("boxing"),
    KARATE("karate"),
    KICKBOXING("kickboxing"),
    MMA("MMA"),
    THAI_BOXING("Thai boxing");

    private final String name;

    Activity(String name) {
        this.name = name;
    }

    public static Activity parseBy(String activityName) {
        Optional<Activity> found = stream(Activity.values())
                .filter((activity -> activity.name.equalsIgnoreCase(activityName)))
                .findFirst();
        return found.orElseThrow(() -> new RuntimeException(String.format("Activity %s not available", activityName)));
    }

    @JsonValue
    public String getName() {
        return name;
    }


}
