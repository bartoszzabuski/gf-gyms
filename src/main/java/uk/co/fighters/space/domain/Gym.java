package uk.co.fighters.space.domain;


import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Document(collection = Gym.GYMS_COLLECTION)
public class Gym {

    public static final String GYMS_COLLECTION = "gyms";

    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;

    private String name;

    private String shortDescription;

    @GeoSpatialIndexed(name = "position")
    private Point position;

    private Set<Activity> activities;

    private Set<Facility> facilities;

    private List<Ranking> rankings;


//    private String mobile;
//    private String websiteUrl;
//    private Boolean featured;

}
