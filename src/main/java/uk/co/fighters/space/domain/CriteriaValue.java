package uk.co.fighters.space.domain;

import lombok.Value;

@Value
public class CriteriaValue {

    private final Integer value;

    public CriteriaValue(){
        this.value = null;
    }

    public CriteriaValue(Integer value){
        if(value < 0 || value > 10){
            throw new RuntimeException("Ranking score can only be between 0 - 10 only");
        }
        this.value = value;
    }
}
