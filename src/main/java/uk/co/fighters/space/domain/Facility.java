package uk.co.fighters.space.domain;

import static java.util.Arrays.stream;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonValue;


public enum Facility {

    BOOT_CAMP("boot camp"),
    CARDIO("cardio"),
    PT("personal trainer"),
    SWIMMING_POOL("swimming pool"),
    WEIGHTS("weights");

    private final String name;

    Facility(String name) {
        this.name = name;
    }

    public static Facility parseBy(String facilityName) {
        Optional<Facility> found = stream(Facility.values())
                .filter((facility -> facility.name.equalsIgnoreCase(facilityName)))
                .findFirst();
        return found.orElseThrow(() -> new RuntimeException(String.format("Facility %s not available", facilityName)));
    }

    @JsonValue
    public String getName() {
        return name;
    }

}

