import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {AgmCoreModule} from "angular2-google-maps/core";
import {MapStylingComponent} from "./map-styling.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SelectButtonModule} from "primeng/components/selectbutton/selectbutton";
import {MapComponent} from "./views/map/map.component";
import {PositionService} from "./services/position.service";
import {FilterComponent} from "./views/filter/filter.compnent";
import {DataGridModule} from "primeng/components/datagrid/datagrid";
import {PanelModule} from "primeng/components/panel/panel";
import {MaterializeModule} from "angular2-materialize/dist";
import {DetailsModalComponent} from "./views/details-modal/details-modal";
import {PairsPipe} from "./utils/pairs.pipe";
import {Angular2SocialLoginModule} from "angular2-social-login";
import {NavBarMenuComponent} from "./views/nav-bar-menu/nav-bar-menu";
import {LoginService} from "./services/login.service";
import {StarRatingComponent} from "./utils/rating/rating.component";
import {GymsService} from "./services/gyms.service.interface";
import {HttpGymsService} from "./services/gyms.service";

let providers = {
  "google": {
    "clientId": "664575810080-0rs7niea9duitv2mtia7ioi4irv21qld.apps.googleusercontent.com"
  },
  "facebook": {
    "clientId": "134362823784359",
    "apiVersion": "v2.8"
  }
};


@NgModule({
  declarations: [
    AppComponent,
    MapStylingComponent,
    MapComponent,
    FilterComponent,
    DetailsModalComponent,
    NavBarMenuComponent,
    PairsPipe,
    StarRatingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    SelectButtonModule,
    DataGridModule,
    PanelModule,
    MaterializeModule,
    Angular2SocialLoginModule,
    // JsonpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC1J04F-VU5pRRw8Dcha9Nr6R1uf4NLkIQ'
    })
  ],
  providers: [PositionService, {provide: 'GymsService', useClass: HttpGymsService}
    , LoginService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

Angular2SocialLoginModule.loadProvidersScripts(providers);
