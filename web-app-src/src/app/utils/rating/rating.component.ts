import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  OnInit,
  OnChanges,
  Renderer,
  AfterViewInit,
  AfterViewChecked,
  AfterContentInit,
  ViewChildren,
  ChangeDetectionStrategy
} from "@angular/core";
import {star} from "./star";
import {RatingEvent} from "./rating-event";

@Component({
  selector: 'star-rating',
  templateUrl: './rating.component.html',
  styles: ['./rating.component.css'],
  host: {},
  // tell Angular to skip change detection for this component’s subtree if none of its inputs changed by
  // setting the change detection strategy to OnPush. that's an performance improvement technique. It forces to pass
  // immutable object as an input and when you want to trigger a change you have to pass a new imuutable object.
  // (with this property set to OnPush you changing properties of mutable input object will not updated view!
  changeDetection: ChangeDetectionStrategy.OnPush

})


export class StarRatingComponent implements OnInit,OnChanges,AfterViewInit,AfterContentInit,AfterViewChecked {

  @Input() ratingId: string;
  @Input() ratingValue: number;
  @Output() ratingValueChange: EventEmitter<number> =
    new EventEmitter<number>();

  @Output() OnRatingChanged = new EventEmitter();

  @Input() max: number;

  @Input() readonly: boolean;

  @Input() starSize: number;

  @Input() color: string;

  @Input() fillColor: string;

  @Output() OnMouseOver = new EventEmitter();

  @ViewChildren('starfilled') starChildren;

  public stars: star[];
  private ele: ElementRef;
  private render: Renderer;
  private changeElements: HTMLElement[];

  constructor(e1: ElementRef, render: Renderer) {
    this.ele = e1;
    this.render = render;
    this.fillColor = "gold";
  }

  public ngOnInit() {

  }

  public ngOnChanges() {
    if (this.readonly == undefined) {
      this.readonly = false;
    }
    if (typeof(this.readonly) == "string") {
      this.readonly = (String(this.readonly) == "true");
    }
    this.updateStars();
  }

  public ngAfterViewInit() {
    this.starChildren.changes.subscribe(changes=> {
      this.changeElements = changes._results.map(
        (eleref)=> eleref.nativeElement
      );

      this.OnRatingChanged.next(new RatingEvent(this.ratingId, this.ratingValue));
    });
  }

  public OnMouseenter(evt) {
    this.OnMouseOver.next(evt);
  }

  public ngAfterViewChecked() {

  }

  public ngAfterContentInit() {

  }

  private updateStars() {
    this.stars = [];

    var j = this.max - this.ratingValue;

    for (var i = this.max; i >= 1; i--) {
      this.stars.push({filled: i > j});
    }
    this.render.setElementStyle(this.ele.nativeElement, 'color',
      this.color);
  }

  private toggle(index) {
    if (this.readonly === false) {
      this.ratingValue = index + 1;
      this.updateStars();
      this.ratingValueChange.emit(this.ratingValue);
    }
  }


}

