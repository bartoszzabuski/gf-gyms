export class RatingEvent {
  constructor(private ratingId: string, private ratingValue: number){}
}
