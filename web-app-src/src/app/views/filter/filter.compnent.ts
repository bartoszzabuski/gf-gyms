import {Component, OnInit, Inject} from "@angular/core";
import {SelectItem} from "primeng/components/common/api";
import {PositionService} from "../../services/position.service";
import {GymsService} from "../../services/gyms.service.interface";

@Component({
  selector: 'filter-view',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})
export class FilterComponent implements OnInit {

  londonAreas: SelectItem[];
  selectedLondonAreas: string;

  activities: SelectItem[];
  selectedActivities: string[] = [];

  facilities: SelectItem[];
  selectedFacilities: string[] = [];

  constructor(@Inject('GymsService') private gymService: GymsService, private positionService: PositionService) {
  }

  ngOnInit(): void {

    console.log("filter component init!!!!");

    this.londonAreas = [];
    this.londonAreas.push({label: 'central', value: 'central'});
    this.londonAreas.push({label: 'west', value: 'west'});
    this.londonAreas.push({label: 'south', value: 'south'});
    this.londonAreas.push({label: 'north', value: 'north'});
    this.londonAreas.push({label: 'east', value: 'east'});

    this.activities = [];
    this.activities.push({label: 'BJJ', value: 'BJJ'});
    this.activities.push({label: 'boxing', value: 'boxing'});
    this.activities.push({label: 'karate', value: 'karate'});
    this.activities.push({label: 'kickboxing', value: 'kickboxing'});
    this.activities.push({label: 'MMA', value: 'MMA'});
    this.activities.push({label: 'Thai boxing', value: 'Thai boxing'});

    this.facilities = [];
    this.facilities.push({label: 'boot camp', value: 'boot camp'});
    this.facilities.push({label: 'cardio', value: 'cardio'});
    this.facilities.push({label: 'personal trainer', value: 'personal trainer'});
    this.facilities.push({label: 'swimming pool', value: 'swimming pool'});
    this.facilities.push({label: 'weights', value: 'weights'});

  }

  updatePosition() {
    this.positionService.update(this.selectedLondonAreas);
  }

  filterGyms() {
    if (!this.selectedActivities.length && !this.selectedFacilities.length) {
      console.log("empty criteria");
      this.gymService.updateGyms();
    } else {
      this.gymService.updateGymsBy(this.selectedActivities, this.selectedFacilities);
    }
  }
}
