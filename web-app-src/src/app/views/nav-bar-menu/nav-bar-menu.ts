import {Component, OnInit, ElementRef, AfterViewInit, Inject, OnDestroy} from "@angular/core";
import {User} from "../../domain/user";
import {LoginService} from "../../services/login.service";
import {Subscription} from "rxjs";
declare var jQuery: any;


@Component({
  selector: 'nav-bar-menu',
  templateUrl: './nav-bar-menu.component.html',
  styleUrls: ['./nav-bar-menu.component.css'],
})
export class NavBarMenuComponent implements OnInit, OnDestroy, AfterViewInit {

  private elementRef: ElementRef;
  private user: User;
  private loggedInUserSubscription: Subscription;


  constructor(@Inject(ElementRef) elementRef: ElementRef, private loginService: LoginService) {
    this.elementRef = elementRef;
    this.loggedInUserSubscription = this.loginService.getLoggedInUserSubject().subscribe(user => this.user = user);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    jQuery(this.elementRef.nativeElement).find('#side-menu-button').sideNav();
  }

  signIn(provider) {
    this.loginService.signIn(provider);
  }

  logout() {
    this.loginService.logout();
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  isUserLoggedIn() {
    return this.loginService.isUserLoggedIn();
  }

  getUser() {
    return this.loginService.getUser();
  }
}
