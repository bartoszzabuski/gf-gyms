import {Component, OnInit, Inject} from "@angular/core";
import {Position} from "../../domain/position";
import {Gym} from "../../domain/gym";
import {PositionService} from "../../services/position.service";
import {Subscription} from "rxjs/Subscription";
import {GymsService} from "../../services/gyms.service.interface";

@Component({
  selector: 'map-view',
  templateUrl: './map.component.html',
  // template: '<div></div>',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  private position: Position;
  private zoom: number;

  private gyms: Gym[];
  private positionSubscription: Subscription;
  private gymsSubscription: Subscription;

  constructor(@Inject('GymsService') private gymService: GymsService, private positionService: PositionService) {
  }

  ngOnInit(): void {
    this.gymsSubscription = this.gymService.getCurrentGymsSubject().subscribe(gyms => this.gyms = gyms);
    this.positionSubscription = this.positionService.getCurrentPositionSubject().subscribe(mapArea => {
      this.position = mapArea.getPosition();
      this.zoom = mapArea.getZoom();
    });
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.positionSubscription.unsubscribe();
    this.gymsSubscription.unsubscribe();
  }

}
