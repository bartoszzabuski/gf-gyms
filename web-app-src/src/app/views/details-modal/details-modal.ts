import {Component, OnInit, EventEmitter, Inject} from "@angular/core";
import {MaterializeAction} from "angular2-materialize/dist";
import {Gym} from "../../domain/gym";
import {Subscription} from "rxjs/Subscription";
import {GymsService} from "../../services/gyms.service.interface";
import {Ranking} from "../../domain/ranking";

@Component({
  selector: 'details-modal',
  templateUrl: './details-modal.component.html',
  styleUrls: ['./details-modal.component.css'],
})
export class DetailsModalComponent implements OnInit {

  private selectedGym: Gym;
  private selectedGymSubscription: Subscription;
//  modal
  modalActions = new EventEmitter<string|MaterializeAction>();

  constructor(@Inject('GymsService') private gymService: GymsService) {
  }

  ngOnInit(): void {
    this.selectedGymSubscription = this.gymService.getSelectedGymSubject().subscribe(gym => {
      this.selectedGym = gym;
      this.openModal();
    });
  }

  ngOnDestroy() {
    this.selectedGymSubscription.unsubscribe();
  }

  openModal() {
    this.modalActions.emit({action: "modal", params: ['open']});
  }

  closeModal() {
    this.modalActions.emit({action: "modal", params: ['close']});
  }

  ratingChanged(event) {
    this.gymService.rateGym(this.selectedGym, new Ranking(event.ratingId, event.ratingValue));
    // console.log(event);
  }
}
