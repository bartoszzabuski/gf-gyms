import {Component, OnInit, OnDestroy, Inject} from "@angular/core";
import {Gym} from "./domain/gym";
import {Subscription} from "rxjs/Subscription";
import {GymsService} from "./services/gyms.service.interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app works!';
  errorMessage: any;

  gyms: Gym[];
  private gymsSubscription: Subscription;

  constructor(@Inject('GymsService') private gymService: GymsService) {
  }

  ngOnInit(): void {
    this.gymsSubscription = this.gymService.getCurrentGymsSubject().subscribe(gyms => this.gyms = gyms);
  }

  ngOnDestroy() {
    this.gymsSubscription.unsubscribe();
  }

  getRandomNumberFor(name: string): string {
    const firstLetter = name.charAt(0).toLocaleLowerCase();
    if ("abcdef".indexOf(firstLetter) > 0) {
      return "1";
    } else if ("ghijkl".indexOf(firstLetter) > 0) {
      return "2";
    } else if ("mnopqr".indexOf(firstLetter) > 0) {
      return "3";
    } else {
      return "4";
    }
  }

  markGymAsSelected(gym: Gym) {
    this.gymService.selectGym(gym);
  }
}
