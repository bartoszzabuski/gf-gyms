import {BehaviorSubject} from "rxjs";
import {Gym} from "../domain/gym";
import {Ranking} from "../domain/ranking";

export interface GymsService {

  updateGyms(): void;

  updateGymsBy(activities: string[], facilities: string[]): void;

  getCurrentGymsSubject(): BehaviorSubject<Gym[]>;

  getSelectedGymSubject(): BehaviorSubject<Gym>;

  selectGym(gym: Gym): void;

  rateGym(gym: Gym, ranking: Ranking): void;

}
