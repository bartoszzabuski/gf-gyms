import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {AuthService} from "angular2-social-login";
import {User} from "../domain/user";

@Injectable()
export class LoginService {

  private loggedOutUser = new User('hello Stranger!', null, null, null, '../../../assets/gym1.jpg', null);

  private _loggedInUser: BehaviorSubject<User>;
  private authServiceSubscription: any;

  constructor(private _auth: AuthService) {
    this.init();
  }

  init() {
    this._loggedInUser = new BehaviorSubject<User>(this.loggedOutUser);
  }

  //consider storing in localStorage or cookie
  //consider sending to backend to be stored
  signIn(provider) {
    this.authServiceSubscription = this._auth.login(provider).subscribe(
      (data) => {
        console.log(data);
        this._loggedInUser.next(this.mapToUser(data));
      }
    )
  }

  private mapToUser(data: Object) {
    return new User(data['name'], data['email'], data['uid'], data['provider'], data['image'], data['token']);
  }

  logout() {
    this._auth.logout().subscribe(
      (data)=> {
        console.log(data);
        this._loggedInUser.next(this.loggedOutUser);
      }
    );
  }

  ngOnDestroy() {
    this.authServiceSubscription.unsubscribe();
  }

  isUserLoggedIn() {
    return this._loggedInUser.getValue() !== this.loggedOutUser;
  }

  getLoggedInUserSubject() {
    return this._loggedInUser;
  }

  getUser() {
    return this._loggedInUser.getValue()
  }
}
