import {Injectable} from "@angular/core";
import {Gym} from "../domain/gym";
import {Observable} from "rxjs";
import {Http, Response} from "@angular/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {GymsService} from "./gyms.service.interface";
import {Ranking} from "../domain/ranking";

@Injectable()
export class HttpGymsService implements GymsService {

  private gymsUrl = 'gyms';
  private londonGymsUrl = this.gymsUrl + '/london';

  private _currentGyms: BehaviorSubject<Gym[]>;
  private _selectedGym: BehaviorSubject<Gym>;

  constructor(private http: Http) {
    this.init();
  }

  init() {
    this._currentGyms = new BehaviorSubject<Gym[]>([]);
    this._selectedGym = new BehaviorSubject<Gym>(new Gym());
    this.getGyms();
  }

  // duplicated structure with updateGyms
  private getGyms() {
    var foundGyms$ = this.http.get(this.londonGymsUrl)
      .map(this.extractData)
      .catch(this.handleError);

    foundGyms$.subscribe(
      gyms => this._currentGyms.next(gyms),
      //  TODO refactor this to use notification bar
      error => console.error(error)
    );
  }

  // duplicated structure with getGyms
  updateGymsBy(activities: string[], facilities: string[]) {
    console.log("querying: " + this.londonGymsUrl + "?activity=" + activities.join(",") + "&facility=" + facilities.join(","));

    let foundGyms = this.http.get(this.londonGymsUrl + "?activity=" + activities.join(",") + "&facility=" + facilities.join(","))
      .map(this.extractData)
      .catch(this.handleError);

    foundGyms.subscribe(
      gyms => this._currentGyms.next(gyms),
      //  TODO refactor this to use notification bar
      error => console.error(error)
    );
  }

  updateGyms() {
    this.getGyms();
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log(body);
    return body || {};
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public rateGym(gym: Gym, ranking: Ranking): void {
    let rate$ = this.http.post(this.gymsUrl + "/" + gym.id + "/ranking", ranking)
      .catch(this.handleError);

    rate$.subscribe(
      response => console.log(response),
      //  TODO refactor this to use notification bar
      error => console.error(error)
    );

    console.log("rate " + rate$ + " " + gym.name + " " + JSON.stringify(ranking));
  }

  public getCurrentGymsSubject(): BehaviorSubject<Gym[]> {
    return this._currentGyms;
  }

  getSelectedGymSubject() {
    return this._selectedGym;
  }

  selectGym(gym: Gym) {
    this._selectedGym.next(gym);
  }

}
