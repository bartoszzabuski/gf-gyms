import {Injectable} from "@angular/core";
import {Position} from "../domain/position";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {MapArea} from "../domain/map-area";

@Injectable()
export class PositionService {

  private initLondonLat: number = 51.507352;
  private initLondonLng: number = -0.127761;
  private initialZoom: number = 10;
  private districtZoom: number = 11;

  private _currentPosition: BehaviorSubject<MapArea>;

  constructor() {
    this.init();
  }

  init(): void {
    const centralLondonCoor = new Position(this.initLondonLat, this.initLondonLng);
    this._currentPosition = new BehaviorSubject<MapArea>(new MapArea(centralLondonCoor, this.initialZoom));
  }

  public update(londonArea: string) {
    console.log("updating to new position: " + londonArea);
    let newPosition: Position;
    if (londonArea == "central") {
      newPosition = new Position(51.5076, -0.1278);
    }
    if (londonArea == "east") {
      newPosition = new Position(51.523325, -0.038110);
    }
    if (londonArea == "north") {
      newPosition = new Position(51.634208, -0.100893);
    }
    if (londonArea == "west") {
      newPosition = new Position(51.514855, -0.304234);
    }
    if (londonArea == "south") {
      newPosition = new Position(51.380733, -0.183989);
    }
    this._currentPosition.next(new MapArea(newPosition, this.districtZoom));
  }

  public getCurrentPositionSubject(): BehaviorSubject<MapArea> {
    return this._currentPosition;
  }

}


