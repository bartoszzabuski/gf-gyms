import {Position} from "./position";

export class MapArea {

  constructor(private position: Position, private zoom: number) {
  }

  public getPosition():Position {
    return this.position;
  }

  public getZoom():number{
    return this.zoom;
  }

}
