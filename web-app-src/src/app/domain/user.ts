export class User {

  constructor(name: string, email: string, uid: string, provider: string, image: string, token: string) {
    this.name = name;
    this.email = email;
    this.uid = uid;
    this.provider = provider;
    this.image = image;
    this.token = token;
  }

  name: string;
  email: string;
  uid: string;
  provider: string;
  image: string;
  token: string
}
