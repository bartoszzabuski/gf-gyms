import {Position} from "./position";
import {Ranking} from "./ranking";

export class Gym {
  id: string;
  name: string;
  shortDescription: string;
  position: Position;
  activities: string[];
  facilities: string[];
  rankings: Ranking[];
}


