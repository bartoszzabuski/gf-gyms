import { GymFinderPage } from './app.po';

describe('gym-finder App', () => {
  let page: GymFinderPage;

  beforeEach(() => {
    page = new GymFinderPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
